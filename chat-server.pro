TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

win32:LIBS += -lws2_32

SOURCES += main.c \
    protocol.c \
    server.c \
    socketarray.c \
    messagelist.c

HEADERS += \
    protocol.h \
    server.h \
    socketarray.h \
    messagelist.h
