#include <stdio.h>
#include <winsock2.h>
#include "server.h"

static void Help()
{
    puts("usage:\nchat-server ADDRESS PORT\n");
}

int main(int argc, char *argv[])
{
    int exitCode = 1;

    if (argc != 3) {
        Help();
    } else {
        WSADATA wsaData;
        int err = WSAStartup(MAKEWORD(2, 2), &wsaData);

        if (err == 0) {
            // TODO Обработать ситуацию, когда в параметре PORT находится не число.
            if (ServerStart(argv[1], atoi(argv[2]), 20)) {
                exitCode = 0;
            }

            err = WSACleanup();
            if (err != 0) {
                printf("error: WSACleanup, code: %d\n", err);
                exitCode = 1;
            }
        } else {
            printf("error: WSAStartup, code: %d\n", err);
        }
    }

    return exitCode;
}
