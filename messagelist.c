#include <string.h>
#include <stdlib.h>
#include "messagelist.h"

message_t MessageInit(const char *data, size_t size)
{
    message_t message = { malloc(size * sizeof(char)), size, 0 };
    memcpy(message.data, data, size * sizeof(char));

    return message;
}

void MessageFree(message_t *message)
{
    if (message) {
        free(message->data);
    }
}

message_list_t MessageListInit()
{
    message_list_t messageList = { NULL };
    return messageList;
}

void MessageListFree(message_list_t *messageList)
{
    message_node_t *node = messageList->node;
    while (node) {
        message_node_t *next = node->next;
        MessageFree(&node->message);
        free(node);
        node = next;
    }
}

bool MessageListEmpty(message_list_t *messageList)
{
    return messageList->node == NULL;
}

void MessageListAppend(message_list_t *messageList, const char *data, size_t size)
{
    if (!messageList->node) {
        // Список пуст
        message_node_t *node = malloc(sizeof(message_node_t));
        node->message = MessageInit(data, size);
        node->next = NULL;
        messageList->node = node;
    } else {
        // В списке есть элементы, поиск последнего
        message_node_t *lastNode = messageList->node;

        while (lastNode->next) {
            lastNode = lastNode->next;
        }

        message_node_t *node = malloc(sizeof(message_node_t));
        node->message = MessageInit(data, size);
        node->next = NULL;
        lastNode->next = node;
    }
}

void MessageListRemoveFirst(message_list_t *messageList)
{
    if (messageList->node) {
        message_node_t *next = messageList->node->next;
        MessageFree(&messageList->node->message);
        free(messageList->node);
        messageList->node = next;
    }
}

message_t * MessageListFirst(message_list_t *messageList)
{
    message_t *message = NULL;

    if (messageList->node) {
        message = &messageList->node->message;
    }

    return message;
}
