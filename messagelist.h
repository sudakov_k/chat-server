#ifndef MESSAGELIST_H
#define MESSAGELIST_H

#include <stddef.h>
#include <stdbool.h>

//!
//! \brief Контейнер - сообщение.
//!
typedef struct {
    char *data;     //!< Текст сообщения.
    size_t size;    //!< Размер собщения.
    size_t sendSize;//!< Размер переданной части.
} message_t;

//!
//! \brief Елемент списка сообщений.
//!
typedef struct message_node_t {
    message_t message;              //!< Сообщение.
    struct message_node_t *next;    //!< Следующий элемент.
} message_node_t;

//!
//! \brief Список сообщений.
//!
typedef struct {
    message_node_t *node;           //!< Первый элемент.
} message_list_t;

#ifdef __cplusplus
extern "C" {
#endif

//!
//! \brief Инициализация сообщения на основе данных.
//! \param data Данные.
//! \param size Размер данных.
//! \return Сообщение.
//!
message_t MessageInit(const char *data, size_t size);

//!
//! \brief Освобождение ресурсов сообщения.
//! \param message Сообщение.
//!
void MessageFree(message_t *message);

//!
//! \brief Инициализация списка сообщений.
//! \return Пустой список сообщений.
//!
message_list_t MessageListInit();

//!
//! \brief Освобождение ресурсов списка сообщений.
//! \messageList Список сообщений.
//!
void MessageListFree(message_list_t *messageList);

//!
//! \brief Проверить список на отсутствие элементов.
//! \param messageList Список сообщений.
//! \return true - список пуст, false - нет.
//!
bool MessageListEmpty(message_list_t *messageList);

//!
//! \brief Добавить сообщение в список.
//! \param messageList Список сообщений.
//! \param data Текст сообщения.
//! \param size Размер сообщения.
//!
void MessageListAppend(message_list_t *messageList, const char *data, size_t size);

//!
//! \brief Удалить первый элемент списка сообщений.
//! \param messageList Список сообщений.
//!
void MessageListRemoveFirst(message_list_t *messageList);

//!
//! \brief Найти первое сообщение.
//! \param messageList Список сообщений.
//! \return Указатель на первое сообщение.
//!
message_t * MessageListFirst(message_list_t *messageList);

#ifdef __cplusplus
}
#endif

#endif // MESSAGELIST_H
