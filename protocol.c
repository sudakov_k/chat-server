#include <stdint.h>
#include <string.h>
#include "protocol.h"

// Преобразование "сырых" данных в тип uint16_t
static uint16_t toU16(char high, char low)
{
    // Преобразование char -> unsigned char необходимо
    // для избежания ошибок преобразования отрицательных чисел
    uint16_t value = (unsigned char) low;
    value |= (((uint16_t) ((unsigned char) high)) << 8) & 0xFF00;

    return value;
}

// Преобразование типа uint16_t в массив данных
// Размер массива: 2 байта
static void fromU16(char *buf, uint16_t value)
{
    buf[0] = (value >> 8) & 0xFF;
    buf[1] = value & 0xFF;
}

// Общая функция для декодирования данных
static bool CommandDecodeData(const char *buf, size_t bufSize, char *data, size_t dataSize)
{
    bool decodeResult = false;  // Результат декодирования.
    size_t minSize = 4;         // Минимальный размер входного буфера.

    if (bufSize >= minSize) {
        uint16_t outSize = toU16(buf[2], buf[3]);   // Размер секции "data"
        minSize += outSize;

        if ((bufSize >= minSize) && (dataSize >= outSize)) {
            memcpy(data, buf + 4, outSize * sizeof(char));
            decodeResult = true;
        }
    }

    return decodeResult;
}

protocol_error_t CommandDecodeInfo(const char *buf, size_t bufSize, client_command_t *clientCommand, size_t *dataSize, size_t *recvSize)
{
    protocol_error_t err = PROTOCOL_ERROR_TRUNC;    // До завершения декодирования считается, что посылка пришла не полностью
    size_t minSize = 2;                             // Минимальный размер посылки

    if (bufSize >= minSize) {
        client_command_t cmd = (client_command_t) toU16(buf[0], buf[1]);

        switch (cmd) {
        case CLIENT_COMMAND_NAME:
        case CLIENT_COMMAND_MESSAGE:
            // Тип посылки - имя клиента или сообщение (обрабатываются одинаково).
            minSize += 2;

            if (bufSize >= minSize) {
                // Размер секции "data".
                uint16_t outSize = toU16(buf[2], buf[3]);
                minSize += outSize;

                if (bufSize >= minSize) {
                    // Выходные параметры будут записаны только если их указатели не равны NULL
                    if (clientCommand) {
                        *clientCommand = cmd;
                    }

                    if (dataSize) {
                        *dataSize = outSize;
                    }

                    err = PROTOCOL_ERROR_OK;
                }
            }

            break;

        default:
            err = PROTOCOL_ERROR_FAIL;
            break;
        }
    }

    // recvSize записывается только если указатель не NULL
    if (recvSize) {
        if (err == PROTOCOL_ERROR_OK) {
            *recvSize = minSize;
        } else {
            *recvSize = 0;
        }
    }

    return err;
}

bool CommandDecodeName(const char *buf, size_t bufSize, char *data, size_t dataSize)
{
    return CommandDecodeData(buf, bufSize, data, dataSize);
}

bool CommandDecodeMessage(const char *buf, size_t bufSize, char *data, size_t dataSize)
{
    return CommandDecodeData(buf, bufSize, data, dataSize);
}

bool CommandEncodeMessage(char *buf, size_t bufSize, const char *name, uint16_t nameSize, const char *message, uint16_t messageSize, size_t *sendSize)
{
    bool encodeReturn = false;

    // Требуемый размер буфера
    size_t outSize = 6 + nameSize + messageSize;

    // Запись требуемого размера буфера
    if (sendSize) {
        *sendSize = outSize;
    }

    // Кодирование сообщения
    if (bufSize >= outSize) {
        char *out = buf;

        fromU16(out, SERVER_COMMAND_MESSAGE);               // cmd
        out += 2;

        fromU16(out, nameSize);                             // name size
        out += 2;

        memcpy(out, name, nameSize * sizeof(char));         // name
        out += nameSize;

        fromU16(out, messageSize);                          // message size
        out += 2;

        memcpy(out, message, messageSize * sizeof(char));   // message
        // out += messageSize
    }

    return encodeReturn;
}
