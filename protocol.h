#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <stddef.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

//!
//! \brief Тип ошибки при декодировании посылки.
//!
typedef enum {
    PROTOCOL_ERROR_OK = 0,              //!< Нет ошибки.
    PROTOCOL_ERROR_TRUNC,               //!< Попытка декодировать часть посылки.
    PROTOCOL_ERROR_FAIL,                //!< Неправильная посылка.
} protocol_error_t;

//!
//! \brief Тип посылки сервера.
//!
typedef enum {
    SERVER_COMMAND_MESSAGE = 1,         //!< Сообщение.
} server_command_t;

//!
//! \brief Тип посылки клиента.
//!
typedef enum {
    CLIENT_COMMAND_NAME = 1,            //!< Имя клиента.
    CLIENT_COMMAND_MESSAGE = 2,         //!< Сообщение.
} client_command_t;

//!
//! \brief Получить информацию о посылке.
//! \param buf Входной буфер.
//! \param bufSize Размер входного буфера.
//! \param clientCommand Тип посылки клиента.
//! \param dataSize Требуемый размер данных секции "data".
//! \param recvSize Размер успешно декодированных данных.
//! \return Ошибка декодирования.
//!
protocol_error_t CommandDecodeInfo(const char *buf, size_t bufSize, client_command_t *clientCommand, size_t *dataSize, size_t *recvSize);

//!
//! \brief Декодировать имя клиента.
//! \param buf Входной буфер.
//! \param bufSize Размер входного буфера.
//! \param data Выходной буфер.
//! \param dataSize Размер выходного буфера.
//! \return Успех операции.
//!
bool CommandDecodeName(const char *buf, size_t bufSize, char *data, size_t dataSize);

//!
//! \brief Декодировать сообщение клиента.
//! \param buf Входной буфер.
//! \param bufSize Размер входного буфера.
//! \param data Выходной буфер.
//! \param dataSize Размер выходного буфера.
//! \return Успех операции.
//!
bool CommandDecodeMessage(const char *buf, size_t bufSize, char *data, size_t dataSize);

//!
//! \brief Кодировать сообщение сервера.
//! \param buf Выходной буфер.
//! \param bufSize Размер выходного буфера.
//! \param name Имя клиента.
//! \param nameSize Размер данных секции "name".
//! \param message Сообщение клиента.
//! \param messageSize Размер данных секции "message".
//! \param sendSize Требуемый размер выходного буфера.
//! \return Успех операции.
//!
//! Если размер выходного буфера мал, то требуемый размер буфера
//! будет записан в переменную "sendSize", а функция вернет "false".
//!
bool CommandEncodeMessage(char *buf, size_t bufSize, const char *name, uint16_t nameSize, const char *message, uint16_t messageSize, size_t *sendSize);

#ifdef __cplusplus
}
#endif

#endif // PROTOCOL_H
