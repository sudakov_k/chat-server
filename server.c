#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include "server.h"
#include "socketarray.h"
#include "messagelist.h"
#include "protocol.h"

static bool ServerNextEvent(SOCKET srvSocket, socket_array_t *clientArray)
{
    if (!clientArray) {
        return false;
    }

    fd_set readSet;
    fd_set writeSet;
    fd_set exceptSet;

    FD_ZERO(&readSet);
    FD_ZERO(&writeSet);
    FD_ZERO(&exceptSet);

    FD_SET(srvSocket, &readSet);
    for (size_t i = 0; i < clientArray->size; ++i) {
        FD_SET(clientArray->data[i], &readSet);

        if (!MessageListEmpty(&clientArray->messageBuf[i])) {
            FD_SET(clientArray->data[i], &writeSet);
        }

        //FD_SET(clientArray->data[i], &exceptSet);
    }

    int result = select(1, &readSet, &writeSet, &exceptSet, NULL);

    if (result != SOCKET_ERROR) {
        // Обработка сообщений от клиентов
        for (size_t i = 0; i < clientArray->size; ++i) {
            if (FD_ISSET(clientArray->data[i], &readSet)) {
                bool isErr = true;

                if (clientArray->recvSize[i] >= clientArray->recvMaxSize) {
                    // Переполнение буфера приема
                    puts("recv buffer overflow\n");
                    closesocket(clientArray->data[i]);
                    SocketArrayRemoveId(clientArray, i);
                    break;
                }

                int recvSize = recv(
                            clientArray->data[i],
                            clientArray->recvBuf[i] + clientArray->recvSize[i],
                            clientArray->recvMaxSize - clientArray->recvSize[i],
                            0);

                switch (recvSize) {
                case 0:
                    // Сокет закрыт
                case SOCKET_ERROR:
                    // Ошибка чтения
                    closesocket(clientArray->data[i]);
                    SocketArrayRemoveId(clientArray, i);
                    break;

                default:
                    // Прием данных завершен, разбор посылки
                    clientArray->recvSize[i] += recvSize;

                    if (clientArray->recvSize[i] > 0) {
                        client_command_t cmd;   // Тип посылки
                        size_t dataSize;        // Размер, необходимый для хранения данных
                        size_t decSize;         // Размер декодированных данных

                        protocol_error_t decErr = CommandDecodeInfo(
                                    clientArray->recvBuf[i],
                                    clientArray->recvSize[i],
                                    &cmd,
                                    &dataSize,
                                    &decSize);

                        switch (decErr) {
                        case PROTOCOL_ERROR_OK:
                            // Посылка пришла полностью
                            isErr = false;

                            switch (cmd) {
                            case CLIENT_COMMAND_NAME:
                                free(clientArray->nameBuf[i]);
                                clientArray->nameBuf[i] = malloc(dataSize * sizeof(char));
                                clientArray->nameSize[i] = dataSize;
                                CommandDecodeName(clientArray->recvBuf[i], clientArray->recvSize[i], clientArray->nameBuf[i], dataSize);

                                if (clientArray->recvSize[i] > decSize) {
                                    memmove(clientArray->recvBuf[i], clientArray->recvBuf[i] + decSize, clientArray->recvSize[i] - decSize);
                                }

                                break;

                            case CLIENT_COMMAND_MESSAGE:
                                {
                                    char *messageBuf = malloc(dataSize * sizeof(char));

                                    // Извлечение сообщения
                                    CommandDecodeMessage(
                                                clientArray->recvBuf[i], clientArray->recvSize[i],
                                                messageBuf, dataSize);

                                    // Формирование сообщения сервера
                                    // Получение размера буфера
                                    size_t srvSize;
                                    CommandEncodeMessage(
                                                NULL, 0,
                                                clientArray->nameBuf[i], clientArray->nameSize[i],
                                                messageBuf, dataSize,
                                                &srvSize);

                                    // Выделение буфера, запись сообщения сервера
                                    char *srvBuf = malloc(srvSize * sizeof(char));
                                    CommandEncodeMessage(
                                                srvBuf, srvSize,
                                                clientArray->nameBuf[i], clientArray->nameSize[i],
                                                messageBuf, dataSize,
                                                NULL);

                                    free(messageBuf);

                                    // Помещение сообщения в список для каждого клиента
                                    for (size_t j = 0; j < clientArray->size; ++j) {
                                        MessageListAppend(&clientArray->messageBuf[j], srvBuf, srvSize);
                                    }

                                    free(srvBuf);
                                }
                                break;

                            default:
                                break;
                            }

                            clientArray->recvSize[i] -= decSize;

                            break;

                        case PROTOCOL_ERROR_TRUNC:
                            // Посылка корректна, но пришла не полностью
                            isErr = false;
                            break;

                        default:
                            closesocket(clientArray->data[i]);
                            SocketArrayRemoveId(clientArray, i);
                            break;
                        }
                    }

                    break;
                }

                if (isErr) {
                    break;
                }
            }
        }

        // Обработка серверных сообщений
        for (size_t i = 0; i < clientArray->size; ++i) {
            if (FD_ISSET(clientArray->data[i], &writeSet)) {
                bool isErr = true;

                // Сервер готов передать данные клиенту, есть не переданные сообщения
                if (!MessageListEmpty(&clientArray->messageBuf[i])) {
                    message_t *message = MessageListFirst(&clientArray->messageBuf[i]);

                    int sendSize = send(
                                clientArray->data[i],
                                message->data + message->sendSize,
                                message->size - message->sendSize,
                                0);

                    switch (sendSize) {
                    case SOCKET_ERROR:
                        closesocket(clientArray->data[i]);
                        SocketArrayRemoveId(clientArray, i);
                        break;

                    default:
                        isErr = false;
                        message->sendSize += sendSize;

                        // Сообщение передано, удаление сообщения из списка.
                        if (message->sendSize >= message->size) {
                            MessageListRemoveFirst(&clientArray->messageBuf[i]);
                        }
                    }
                }

                if (isErr) {
                    break;
                }
            }
        }

        // Обработка исключений
        // сейчас исключения невозможны так, как для их возникновения
        // необходимо создавать сокеты со специальными флагами
        //for (size_t i = 0; i < clientArray->size; ++i) {
        //    if (FD_ISSET(clientArray->data[i], &exceptSet)) {
        //        printf("client %d ready to except\n", i);
        //    }
        //}

        // Новое подключение
        if (FD_ISSET(srvSocket, &readSet)) {
            SOCKET clientSocket = accept(srvSocket, NULL, NULL);

            if (clientSocket != INVALID_SOCKET) {
                if (!SocketArrayFull(clientArray)) {
                    // Есть доступные подключения
                    // Переключение сокета в неблокирующий режим
                    u_long nonBlock = 1;
                    int err = ioctlsocket(clientSocket, FIONBIO, &nonBlock);

                    if (err != SOCKET_ERROR) {
                        printf("client connected\n");

                        // Добавить сокет клиента в массив
                        SocketArrayAppend(clientArray, clientSocket);
                    } else {
                        printf("error: switch client socket to nonblock, code: %d\n", WSAGetLastError());
                    }
                } else {
                    // Максимальное количество подключений
                    closesocket(clientSocket);
                }
            } else {
                printf("error: accept socket, code: %d\n", WSAGetLastError());
            }
        }
    } else {
        printf("error: select, code: %d\n", WSAGetLastError());
    }

    return true;
}

bool ServerStart(const char *host, uint16_t port, int maxClients)
{
    bool srvResult = false;

    if (maxClients < (FD_SETSIZE - 1)) {
        // Массив сокетов для клиентов
        socket_array_t clientArray = SocketArrayInit(maxClients, 1024 * 1024, 1024 * 1024);

        SOCKET srvSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

        if (srvSocket != INVALID_SOCKET) {
            // Сетевые параметры сервера
            struct sockaddr_in service;
            memset(&service, 0, sizeof(struct sockaddr_in));

            service.sin_family = AF_INET;
            service.sin_addr.s_addr = inet_addr(host);
            service.sin_port = htons(port);

            // Связывание сокета с сетевым интерфейсом
            int err = bind(srvSocket, (const struct sockaddr *) &service, sizeof(struct sockaddr_in));
            if (err != SOCKET_ERROR) {
                // Прослушивание порта
                if (listen(srvSocket, SOMAXCONN) != SOCKET_ERROR) {
                    // Переключение сокета в неблокирующий режим
                    // необходимо для неблокирующего вызова "accept"
                    u_long nonBlock = 1;
                    err = ioctlsocket(srvSocket, FIONBIO, &nonBlock);

                    if (err != SOCKET_ERROR) {
                        // Ожидание подключений и их обработка
                        while (ServerNextEvent(srvSocket, &clientArray));

                        // Завершение работы
                        srvResult = true;
                    } else {
                        printf("error: switch server socket to nonblock, code: %d\n", WSAGetLastError());
                    }
                } else {
                    printf("error: listen server socket, code: %d\n", WSAGetLastError());

                    err = closesocket(srvSocket);
                    if (err == SOCKET_ERROR) {
                        printf("error: close server socket, code: %d\n", WSAGetLastError());
                    }
                }
            } else {
                printf("error: bind server socket, code: %d\n", WSAGetLastError());

                err = closesocket(srvSocket);
                if (err == SOCKET_ERROR) {
                    printf("error: close server socket, code: %d\n", WSAGetLastError());
                }
            }
        } else {
            printf("error: create server socket, code: %d\n", WSAGetLastError());
        }

        SocketArrayFree(&clientArray);
    } else {
        printf("error: maximum clients is %d\n", (int) (FD_SETSIZE - 1));
    }

    return srvResult;
}
