#ifndef SERVER_H
#define SERVER_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

//!
//! \brief Начало работы сервера.
//! \param host Интерфейс сервера.
//! \param port Порт сервера.
//! \param maxClients Максимальное количество подключений.
//! \return Успех операции.
//!
bool ServerStart(const char *host, uint16_t port, int maxClients);

#ifdef __cplusplus
}
#endif

#endif // SERVER_H
