#include "socketarray.h"

socket_array_t SocketArrayInit(size_t maxSize, size_t recvSize, size_t sendSize)
{
    // Память для буфера приема и передачи не выделяется
    socket_array_t socketArray = {
        malloc(maxSize * sizeof(SOCKET)), 0, maxSize,
        malloc(maxSize * sizeof(char*)), malloc(maxSize * sizeof(size_t)), recvSize,
        malloc(maxSize * sizeof(char*)), malloc(maxSize * sizeof(size_t)), sendSize,
        malloc(maxSize * sizeof(char*)), malloc(maxSize * sizeof(size_t)),
        malloc(maxSize * sizeof(message_list_t))
    };

    return socketArray;
}

void SocketArrayFree(socket_array_t *socketArray)
{
    // Удаление оставшихся элементов
    while (socketArray->size > 0) {
        SocketArrayRemoveId(socketArray, socketArray->size - 1);
    }

    // Освобождение остальных ресурсов
    free(socketArray->data);
    free(socketArray->recvBuf);
    free(socketArray->recvSize);
    free(socketArray->sendBuf);
    free(socketArray->sendSize);
    free(socketArray->nameBuf);
    free(socketArray->nameSize);
    free(socketArray->messageBuf);
}

bool SocketArrayFull(socket_array_t *socketArray)
{
    return socketArray->size >= socketArray->maxSize;
}

bool SocketArrayAppend(socket_array_t *socketArray, SOCKET value)
{
    bool isAppend = false;

    if (socketArray->size < socketArray->maxSize) {
        socketArray->data[socketArray->size] = value;
        socketArray->recvBuf[socketArray->size] = malloc(socketArray->recvMaxSize * sizeof(char));
        socketArray->recvSize[socketArray->size] = 0;
        socketArray->sendBuf[socketArray->size] = malloc(socketArray->sendMaxSize * sizeof(char));
        socketArray->sendSize[socketArray->size] = 0;
        socketArray->nameBuf[socketArray->size] = NULL;
        socketArray->nameSize[socketArray->size] = 0;
        socketArray->messageBuf[socketArray->size] = MessageListInit();
        ++socketArray->size;

        isAppend = true;
    }

    return isAppend;
}

void SocketArrayRemoveId(socket_array_t *socketArray, size_t id)
{
    if (id < socketArray->size) {
        // Освобождения буфера приема и передачи
        free(socketArray->recvBuf[id]);
        free(socketArray->sendBuf[id]);

        // Освобождение буфера имени
        free(socketArray->nameBuf[id]);
        socketArray->nameSize[id] = 0;

        // Освобождение списка сообщений
        MessageListFree(&socketArray->messageBuf[id]);

        for (size_t i = id; i < (socketArray->size - 1); ++i) {
            socketArray->data[i] = socketArray->data[i + 1];
            socketArray->recvBuf[i] = socketArray->recvBuf[i + 1];
            socketArray->recvSize[i] = socketArray->recvSize[i + 1];
            socketArray->sendBuf[i] = socketArray->sendBuf[i + 1];
            socketArray->sendSize[i] = socketArray->sendSize[i + 1];
            socketArray->nameBuf[i] = socketArray->nameBuf[i + 1];
            socketArray->nameSize[i] = socketArray->nameSize[i + 1];
            socketArray->messageBuf[i] = socketArray->messageBuf[i + 1];
        }

        --socketArray->size;
    }
}
