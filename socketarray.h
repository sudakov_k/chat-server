#ifndef SOCKETARRAY_H
#define SOCKETARRAY_H

#include <winsock2.h>
#include <windows.h>
#include <stddef.h>
#include <stdbool.h>
#include "messagelist.h"

//!
//! \brief Массив с сокетами.
//!
typedef struct {
    SOCKET *data;       //!< Данные.
    size_t size;        //!< Размер массива.
    size_t maxSize;     //!< Максимальный размер.

    char **recvBuf;     //!< Буфер приемника.
    size_t *recvSize;   //!< Размер буфера приемника.
    size_t recvMaxSize; //!< Максимальный размер буфера приемника.

    char **sendBuf;     //!< Буфер передатчика.
    size_t *sendSize;   //!< Размер буфера передатчика.
    size_t sendMaxSize; //!< Максимальный размер буфера передатчика.

    char **nameBuf;     //!< Буфер имени клиента.
    char *nameSize;     //!< Размер имени клиента.

    message_list_t *messageBuf; //!< Буфер списка сообщений.
} socket_array_t;

#ifdef __cplusplus
extern "C" {
#endif

//!
//! \brief Создает массив с сокетами.
//! \param maxSize Максимальный размер массива.
//! \param maxSize Максимальный размер буфера приемника.
//! \param maxSize Максимальный размер буфера передатчика.
//! \return Инициализированная структура.
//!
socket_array_t SocketArrayInit(size_t maxSize, size_t recvSize, size_t sendSize);

//!
//! \brief Освобождает ресурсы массива с сокетами.
//! \param Инициализированная структура.
//!
void SocketArrayFree(socket_array_t *socketArray);

//!
//! \brief Проверка массива на заполнение.
//! \param socketArray Массив.
//! \return true - массив заполнен, false - нет.
//!
bool SocketArrayFull(socket_array_t *socketArray);

//!
//! \brief Добавить сокет в массив.
//! \param socketArray Массив.
//! \param value Сокет.
//! \return Успех операции.
//!
bool SocketArrayAppend(socket_array_t *socketArray, SOCKET value);

//!
//! \brief Удалить елемент по его номеру.
//! \param socketArray Массив.
//! \param id Номер удаляемого элемента.
//!
void SocketArrayRemoveId(socket_array_t *socketArray, size_t id);

#ifdef __cplusplus
}
#endif

#endif // SOCKETARRAY_H
